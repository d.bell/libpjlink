#include "Connection.hpp"
#include "TcpConnection.hpp"

namespace Pj {

    TcpConnection::TcpConnection(std::string D_address)
    :Connection(D_address,true) {
    }

    TcpConnection::~TcpConnection() {
        if (D_socketfd > 0)
            close(D_socketfd);

        freeaddrinfo(D_address);
    }

    void TcpConnection::connect() {
        struct addrinfo *rp;
        if (D_address != nullptr) {
            for (rp = D_address; rp; rp = rp->ai_next)
            {
                D_socketfd = ::socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
                if (D_socketfd == -1)
                    continue;
                int res = ::connect(D_socketfd, rp->ai_addr, rp->ai_addrlen);
                if (res != -1) {
                    isConnected = true;
                    break;
                }

                std::cout << res << std::endl;
            }

            if (rp == 0) {
                perror("s:25");
            }
        }
    }

      int TcpConnection::sendCom(const char* c) {
          int ret = send(D_socketfd, c, sizeof(c),0);
          std::cout << "send";
          return ret;
      }
}