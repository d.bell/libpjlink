#include "PJLink.hpp"


namespace Pj {

PJLink::PJLink(std::string address)
:_address(address)
{
    _useAuth = false;
    init();
}

PJLink::PJLink(std::string address, std::string passwd)
:_address(address)
{
      _useAuth = false;
      init();
}

PJLink::PJLink(std::string address, int port)
:_address(address)
{
      _useAuth = false;
      init();
}

PJLink::PJLink(std::string address, int port, std::string passwd)
:_address(address)
{
    _useAuth = (passwd != "");
   init();
}

void PJLink::init() {

    tcpSocket = new TcpConnection(_address);

}

/**
 * @brief Pjlink::connect
 */
void PJLink::connect()
{
    if (!tcpSocket->isConnected)
        tcpSocket->connect();

}

void PJLink::sendCommand(const char* command){
    tcpSocket->sendCom(command);

}

/**
 * @brief Pjlink::read
 */
void PJLink::read() {

}

/**
 * @brief Pjlink::keepAlive
 */
void PJLink::keepAlive(){

}




}