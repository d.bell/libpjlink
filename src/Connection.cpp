#include "settings.hpp"
#include "Connection.hpp"

namespace Pj{
    Connection::Connection(std::string address, bool tcp) {
        setAddress(address, tcp);
    }

    Connection::~Connection() {
 freeaddrinfo(D_address);
    }

    void Connection::connect() {

    }

    void Connection::disconnect() {
#ifdef _WIN32
        closesocket(sock);
#else
        close(sock);
#endif
   isConnected = false;
    }

    void Connection::setAddress(const std::string& s){
        _address = s;
    }

void Connection::setAddress(std::string s, bool tcp) {
    struct addrinfo hints;

    std::memset(&hints, 0, sizeof(hints));

    hints.ai_family = AF_UNSPEC;
    hints.ai_flags = AI_ADDRCONFIG;

    if (tcp)
        hints.ai_socktype = SOCK_STREAM;
    else
        hints.ai_socktype = SOCK_DGRAM;

    int err = getaddrinfo(s.data(), PJLINK_PORT, &hints, &D_address);
    if (err) {
        if (err == EAI_SYSTEM) {
            fprintf(stderr, "%s:25: %s\n",s.data(), strerror(errno));
        } else {
            fprintf(stderr, "%s:25: %s\n", s.data(), gai_strerror(err));
        }

    }
}

    void Connection::setPort(const std::string& s) {
        _port = s;
    }

    bool Connection::sendCommand(std::string command){

        // return send(sock, command.data(), command.length, 0);
        return true;
    }

}