#pragma once
#include "Connection.hpp"

namespace Pj {

class TcpConnection : public Connection {

    public:
        TcpConnection(std::string D_address);
        ~TcpConnection();

        void connect();
        int sendCom(const char* c);

    private:


};

}