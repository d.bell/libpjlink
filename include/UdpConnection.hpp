#pragma once
#include "Connection.hpp"

namespace Pj {

class UdpConnection : public Connection {

    public:
        UdpConnection(std::string D_address);
        ~UdpConnection();

        void connect();

    private:
};

}