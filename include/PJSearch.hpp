#pragma once
#include "UdpConnection.hpp"

namespace Pj {

class PJSearch {

    public:
    #ifdef _WIN32
    __declspec(dllexport)
    #endif
    PJSearch();

    #ifdef _WIN32
    __declspec(dllexport)
    #endif
    ~PJSearch();

    #ifdef _WIN32
    __declspec(dllexport)
    #endif
    void search();

    private:
        UdpConnection *connection = nullptr;


};
}