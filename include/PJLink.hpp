#pragma once
#include "settings.hpp"
#include "TcpConnection.hpp"

namespace Pj {

class PJLink {

public:
    PJLink(std::string address);
    PJLink(std::string address, std::string passwd);
    PJLink(std::string address, int port);
    PJLink(std::string address, int port, std::string passwd);
    ~PJLink(){

    }

    // struct sockaddr_in* getAddress(){return address;}
    const char* getPort(){return _port;}
    bool usingAuth(){return _useAuth;}
    std::string getPasswd(){return _passwd;}
    std::string getpjKey(){return _pjKey;}

    void init();
    void connect();
    bool isConnected() { return tcpSocket->isConnected; }
    void sendCommand(const char* command);

    void read();
    void keepAlive();

private:
  TcpConnection *tcpSocket = nullptr;

  int socketfd;
  const char* _port = PJLINK_PORT;

  bool _useAuth = false;
  bool persistant = true;

  std::string _address;
  std::string _passwd = "";
  std::string _pjKey = "";

  char *received;

};


}