#pragma once
#include <iostream>
#include <stdlib.h>
#include <string>


#ifdef _WIN32
#include <winsock2.h>
#pragma comment(lib,"ws2_32.lib") //Winsock Library
#else
#include <string>
#include <iostream>
#include <arpa/inet.h>
#include <errno.h>
#include <getopt.h>
#include <netdb.h>
#include <sys/socket.h>
#endif

namespace Pj {

class Connection {
    public:
        #pragma region constructors

        Connection(std::string address, bool tcp);
        virtual ~Connection();
        #pragma endregion

        virtual void connect();
        void disconnect();

        #pragma region getters/setters
        const std::string& getAddress(){return _address;}
        const std::string& getPort(){return _port;}

        void setAddress(const std::string& s);
        void setAddress(std::string s, bool tcp);
        void setPort(const std::string &s);
#pragma endregion

        bool isConnected = false;
        bool sendCommand(std::string command);

        struct addrinfo *D_address;
        int D_socketfd;
        std::string D_port;
    private:

        std::string _port;
        std::string _address;

#ifdef _WIN32
        WSADATA wsa;
        SOCKET sock;
#else
        int sock;
#endif

};

}