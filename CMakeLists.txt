# CMakeList.txt : CMake project for HelloWorld, include source and define
# project specific logic here.
#
cmake_minimum_required (VERSION 3.8)

project("libpjlink")

SET (${PROJECT}_VERSION_MAJOR 1)
SET (${PROJECT}_VERSION_MINOR 0)

FILE(GLOB SOURCES "src/*.cpp")

add_library(${PROJECT_NAME} SHARED ${SOURCES})

target_compile_features(${PROJECT_NAME} PRIVATE cxx_range_for)
target_include_directories (${PROJECT_NAME} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/include)


